Syntax: site take username credits

Takes credits credits from user username
Note: adding an optional K, M or G at the end represents
the size in kilo-, mega- and gigabyte respectively
username can be '*' to indicate ALL users

Requires: UserPermission.CREDITS