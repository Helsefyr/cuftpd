Syntax: site give username credits[K|M|G]

Adds 'credits' credits to user 'username'
Note: adding an optional K, M or G at the end represents 
the size in kilo-, mega- and gigabyte respectively
username can be '*' to indicate ALL users

Requires: UserPermission.USEREDIT