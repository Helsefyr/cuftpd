Syntax: site adduser username password [ident@ip]*

Adds a user to the system with the specified username, 
password and ident@ip hostmasks
Requires: UserPermission.ADDUSER