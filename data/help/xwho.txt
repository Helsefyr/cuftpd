Syntax: site xwho

Lists all currently logged on users
and their connection ids.
Requires: UserPermission.KICK
(Requires KICK because the sole reason
to use this instead of "site who" is
to kick users)