Syntax: site shutdown

Shuts down the server.

Requires: UserPermission.SHUTDOWN