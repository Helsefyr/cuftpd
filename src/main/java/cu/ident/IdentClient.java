/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ident;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-maj-15 : 19:43:50
 * @version $Id: IdentClient.java 258 2008-10-26 12:47:23Z jevring $
 */
public class IdentClient {
    private static final int CONNECTION_TIMEOUT = 3000; // was 7 seconds, but cubnc defaults to 3, so lets use that for now

    public String getIdent(Socket client) {
        Socket ident = null;
        PrintWriter out = null;
        BufferedReader in = null;
        try {
            ident = new Socket();
	        ident.setTcpNoDelay(true);
            ident.connect(new InetSocketAddress(client.getInetAddress(), 113), CONNECTION_TIMEOUT);
            out = new PrintWriter(ident.getOutputStream(), true);
            out.print(client.getPort() + " , " +  client.getLocalPort() + "\r\n");
            out.flush();

            in = new BufferedReader(new InputStreamReader(ident.getInputStream()));
            String response = in.readLine();
            return parseIdent(response);
        } catch (IOException e) {
            //System.err.println("Failed to get ident from: " + client.getInetAddress());
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    // lib classes don't use stuff from the main app
                    e.printStackTrace();
                }
            }
            if (out != null) {
                out.close();
            }
            if (ident != null) {
                try {
                    ident.close();
                } catch (IOException e) {
                    // lib classes don't use stuff from the main app
                    e.printStackTrace();
                }
            }
        }
        return "*";
    }

    /**
     * Internal method that parses the ident response string and gets the username.
     * @param response what to parse.
     * @return the username.
     */
    private String parseIdent(String response) {
        if (response != null) {
            // because this needs to be fast, as soon as we don't get exactly what we want, we just default to "*"
            String[] ident = response.split(":");
            // 6193, 23 : USERID : UNIX : stjohns
            if (ident.length == 4) {
                if ("USERID".equalsIgnoreCase(ident[1].trim())) {
                    return ident[3].trim();
                }
            }
        }
        return "*";
    }
}
