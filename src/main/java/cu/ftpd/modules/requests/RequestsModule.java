/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.ftpd.modules.requests;

import cu.ftpd.modules.Module;
import cu.ftpd.modules.requests.actions.*;
import cu.ftpd.events.EventHandler;
import cu.ftpd.commands.site.SiteCommandHandler;
import cu.ftpd.modules.requests.actions.Approve;
import cu.ftpd.commands.site.actions.Action;
import cu.settings.ConfigurationException;
import cu.settings.XMLSettings;

import java.io.IOException;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2008-okt-02 - 10:41:27
 * @version $Id: RequestsModule.java 258 2008-10-26 12:47:23Z jevring $
 */
public class RequestsModule implements Module {
    private RequestLog requestLog = null;
    public void initialize(XMLSettings settings) throws ConfigurationException {
        try {
            requestLog = new RequestLog(settings.get("/requests/log"), settings.getBoolean("/requests/auto_approve"));
        } catch (IOException e) {
            throw new ConfigurationException("Failed to load requestlog: " + e.getMessage(), e);
        }
    }

    public void registerActions(SiteCommandHandler siteCommandHandler) {
        // _todo: only register if auto_approve == false
        // no, because then it becomes impossible to approve requests that were added before auto_approve was turned on
        siteCommandHandler.registerAction("approve", new Approve(requestLog));
        siteCommandHandler.registerAction("request", new cu.ftpd.modules.requests.actions.Request(requestLog));


        siteCommandHandler.registerAction("requests", new Requests(requestLog));
        Action req = new ReqFilled(requestLog);
        siteCommandHandler.registerAction("reqfilled", req);
        siteCommandHandler.registerAction("fillrequest", req);
    }

    public void registerEventHandlers(EventHandler eventHandler) {
    }

    public void stop() {
    }
}
