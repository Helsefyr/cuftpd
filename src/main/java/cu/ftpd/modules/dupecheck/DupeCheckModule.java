/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.ftpd.modules.dupecheck;

import cu.ftpd.modules.Module;
import cu.ftpd.modules.dupecheck.actions.Undupe;
import cu.ftpd.modules.dupecheck.eventhandler.DupelogHandler;
import cu.ftpd.Server;
import cu.ftpd.persistence.Saver;
import cu.ftpd.events.EventHandler;
import cu.ftpd.events.Event;
import cu.ftpd.commands.site.SiteCommandHandler;
import cu.settings.ConfigurationException;
import cu.settings.XMLSettings;

import java.io.IOException;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2008-okt-02 - 09:45:45
 * @version $Id: DupeCheckModule.java 258 2008-10-26 12:47:23Z jevring $
 */
public class DupeCheckModule implements Module {
    private Dupelog dupelog = null;

    public void initialize(XMLSettings settings) throws ConfigurationException {
        String log = settings.get("/dupecheck/log");
        if (log == null || "".equals(log) || "none".equals(log)) {
            dupelog = new DummyDupelog();
        } else {
            try {
                dupelog = new Dupelog(log, settings.getInt("/dupecheck/days"), settings.getBoolean("/dupecheck/case_sensitive"), settings.get("/dupecheck/files"));
                dupelog.load();
                // save and clean out old dupes every hour (it is also done on load and 'site undupe'
                Server.getInstance().getTimer().schedule(new Saver(dupelog), 3600*1000, 3600*1000);
            } catch (IOException e) {
                throw new ConfigurationException("Failed to load dupelog: " + e.getMessage(), e);
            }
        }
    }

    public void registerActions(SiteCommandHandler siteCommandHandler) {
        Undupe undupe = new Undupe(dupelog);
        siteCommandHandler.registerAction("undupe", undupe);
    }

    public void registerEventHandlers(EventHandler eventHandler) {
        DupelogHandler dph = new DupelogHandler(dupelog);

        eventHandler.addAfterEventHandler(Event.DELETE_FILE, dph);      // remove from dupelog
        eventHandler.addAfterEventHandler(Event.UPLOAD, dph);           // add to dupelog
        eventHandler.addBeforeEventHandler(Event.UPLOAD, dph);          // dupecheck
    }

    public void stop() {
        if (dupelog != null) {
            dupelog.shutdown();
        }
    }
}
