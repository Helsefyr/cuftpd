/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.ftpd.modules.dupecheck.eventhandler;

import cu.ftpd.events.AfterEventHandler;
import cu.ftpd.events.Event;
import cu.ftpd.events.BeforeEventHandler;
import cu.ftpd.modules.dupecheck.Dupe;
import cu.ftpd.modules.dupecheck.Dupelog;
import cu.ftpd.logging.Formatter;
import cu.ftpd.filesystem.permissions.ActionPermission;
import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.Connection;
import cu.ftpd.Server;
import cu.ftpd.ServiceManager;
import cu.shell.ProcessResult;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2008-jan-26 - 11:30:20
 * @version $Id: DupelogHandler.java 294 2009-03-04 22:10:42Z jevring $
 */
public class DupelogHandler implements AfterEventHandler, BeforeEventHandler {
    private final Dupelog dupelog;

    public DupelogHandler(Dupelog dupelog) {
        this.dupelog = dupelog;
    }

    public void handleEvent(Event event) {
        final String path = event.getProperty("file.path.ftp");
        String filename = FileSystem.getNameFromPath(path);
        String parentDirectory = FileSystem.getParentDirectoryFromPath(path);
        if (ServiceManager.getServices().getPermissions().shouldLog(ActionPermission.DUPELOG, parentDirectory, event.getUser())) {
            String exitValue = event.getProperty("transfer.postprocessor.exitvalue");
            // this will not throw a NumberFormatException for trying to parse null, as we are checking for it before
            boolean crcOk = exitValue != null && Integer.parseInt(exitValue) == ProcessResult.OK.exitvalue;
            if (event.getType() == Event.DELETE_FILE) {
                dupelog.removeDupe(filename);
            } else if (event.getType() == Event.UPLOAD && crcOk) {
                dupelog.addDupe(event.getUser().getUsername(), filename);
            }
        }
    }

    public boolean handleEvent(Event event, Connection connection) {
        final String path = event.getProperty("file.path.ftp");
        String filename = FileSystem.getNameFromPath(path);
        String parentDirectory = FileSystem.getParentDirectoryFromPath(path);
        if (ServiceManager.getServices().getPermissions().shouldLog(ActionPermission.DUPECHECK, parentDirectory, event.getUser())) {
            Dupe dupe = dupelog.getDupe(filename);
            if (dupe != null) {
                // wouldn't it be nice (and memory intensive) to have the path to the file here too?
                // the problem is that whenever it was moved, we'd have to update the object =(
                connection.respond("553 This looks like a dupe! It was uploaded by " + dupe.getUsername() + ' ' + Formatter.shortDuration((System.currentTimeMillis() - dupe.getTime()) / 1000) + " ago");
                return false;
            } // if it returns null, we're happy
        }
        return true;
    }
}
