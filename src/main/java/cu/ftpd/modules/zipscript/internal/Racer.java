/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.modules.zipscript.internal;

import java.io.Serializable;
import java.util.HashMap;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-maj-27 : 12:27:37
 * @version $Id: Racer.java 258 2008-10-26 12:47:23Z jevring $
 */
public class Racer implements Serializable, Comparable {
    private final String username;
    private final String group; // just to have faster access to it.
    private final HashMap<String, RaceFile> files;
    private long bytesRaced; // used to get average speed
    private long timeTaken; // used to get average speed


    public Racer(String username,String group) {
        files = new HashMap<String, RaceFile>();
        this.username = username;
        this.group = group;
    }

    public void addFile(RaceFile file) {
        files.put(file.getName(), file);
        bytesRaced += file.getSize();
        timeTaken += file.getTime();
    }
    public void deleteFile(RaceFile file) {
        files.remove(file.getName());
        bytesRaced -= file.getSize();
        timeTaken -= file.getTime();
    }
    public int getNumberOfFiles() {
        return files.size();
    }

    /**
     * Returns the speed (in kilobytes per second) as a long.
     * This is the combined speed of all the transfers so far for this user.
     * Basically it's [all bytes uploaded] / [the time it took to upload them]
     *
     * @return the speed of this racer in kilobytes per second.
     */
    public long getSpeed() {
        double dtime = (double)timeTaken / 1000.0d; // seconds, not milliseconds
        return (long)(((double)bytesRaced/ 1024.0D) / dtime);
    }

    public String getGroup() {
        return group;
    }

    public RaceFile getFile(String filename) {
        return files.get(filename);
    }

    public long getBytesRaced() {
        return bytesRaced;
    }

    public long getTimeTaken() {
        return timeTaken;
    }

    public String getUsername() {
        return username;
    }

    public int compareTo(Object o) {
        // _todo: sort on amount of data uploaded instead?
        /*
        _todo: userstats doesn't announce in order (they are, but based on the number of files, not the amount of bytes)
        [18:43:34] <Alix> also, userstats doesn't announce in order
        [18:43:58] <Alix> well, I mean in order of bytes sent

         */
        /*
        Racer r = (Racer)o;
        if (getNumberOfFiles() > r.getNumberOfFiles()) {
            return 1;
        } else if (getNumberOfFiles() < r.getNumberOfFiles()) {
            return -1;
        } else {
            return 0;
        }
         */
        Racer r = (Racer)o;
        if (getBytesRaced() > r.getBytesRaced()) {
            return 1;
        } else if (getBytesRaced() < r.getBytesRaced()) {
            return -1;
        } else {
            return 0;
        }
    }
}
