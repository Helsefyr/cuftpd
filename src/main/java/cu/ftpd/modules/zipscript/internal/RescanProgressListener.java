package cu.ftpd.modules.zipscript.internal;

import java.io.File;

/**
 * Listener used when requesting progress about a {@code site rescan}.
 *
 * @author markus@jevring.net
 */
public interface RescanProgressListener {
	/**
	 * Notifies a listener that a file was scanned.
	 *
	 * @param file    the file that was scanned
	 * @param ok      {@code true} if the file passed, otherwise {@code false}
	 * @param scanned how many files have been scanned so far
	 * @param total   how many files there are in the directory
	 */
	public void fileScanned(File file, boolean ok, int scanned, int total);
}
