/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.modules.zipscript.internal;

import java.io.Serializable;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-maj-27 : 12:38:31
 * @version $Id: RaceGroup.java 258 2008-10-26 12:47:23Z jevring $
 */
public class RaceGroup implements Serializable, Comparable {
    private final String name;
    private int numberOfFiles;
    private long timeTaken;
    private long bytesTransferred;


    public RaceGroup(String name) {
        this.name = name;
    }

    public void addFile(RaceFile file) {
        numberOfFiles++;
        bytesTransferred += file.getSize();
        timeTaken += file.getTime();
    }
    public void deleteFile(RaceFile file) {
        numberOfFiles--;
        bytesTransferred -= file.getSize();
        timeTaken -= file.getTime();
    }

    public String getName() {
        return name;
    }

    public int getNumberOfFiles() {
        return numberOfFiles;
    }

    public long getTimeTaken() {
        return timeTaken;
    }

    public long getBytesTransferred() {
        return bytesTransferred;
    }

    public long getSpeed() {
        double dtime = (double)timeTaken / 1000.0d; // seconds, not milliseconds
        return (long)(((double)bytesTransferred/ 1024.0D) / dtime);
    }

    public int compareTo(Object o) {
        RaceGroup rg = (RaceGroup)o;
        if (numberOfFiles > rg.getNumberOfFiles()) {
            return 1;
        } else if (numberOfFiles < rg.getNumberOfFiles()) {
            return -1;
        } else {
            return 0;
        }
    }
}
