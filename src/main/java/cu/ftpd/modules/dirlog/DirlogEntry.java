/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.modules.dirlog;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-maj-22 : 19:06:01
 * @version $Id: DirlogEntry.java 258 2008-10-26 12:47:23Z jevring $
 */
public class DirlogEntry {
    private String path;
    private final long time;
    private long size;
    private int files;
    private final String username;

    public DirlogEntry(long time, String username, int files, long size, String path) {
        this.username = username;
        this.path = path;
        this.time = time;
        this.size = size;
        this.files = files;
    }

    public DirlogEntry(String path, String username) {
        this.username = username;
        this.path = path;
        this.time = System.currentTimeMillis();
    }

    public void addFile(long size) {
        this.size += size;
        this.files++;
    }

    public String getPath() {
        return path;
    }

    public long getTime() {
        return time;
    }

    public long getSize() {
        return size;
    }

    public int getFiles() {
        return files;
    }

    public String getUsername() {
        return username;
    }

    public void delFile(long bytesToRemove) {
        size -= bytesToRemove;
        files--;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String toString() {
        // NOTE: for some reason we must use ";" here instead of ';', otherwise the ; disappears
        return time + ";" + username + ';' + files + ';' + size + ';' + path;
    }
}
