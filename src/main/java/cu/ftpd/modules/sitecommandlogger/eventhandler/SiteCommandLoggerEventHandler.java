/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.ftpd.modules.sitecommandlogger.eventhandler;

import cu.ftpd.events.BeforeEventHandler;
import cu.ftpd.events.Event;
import cu.ftpd.Connection;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.util.Locale;
import java.io.PrintWriter;
import java.text.MessageFormat;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version $Id: SiteCommandLoggerEventHandler.java 258 2008-10-26 12:47:23Z jevring $
 * @since 2008-okt-03 - 17:40:38
 */
public class SiteCommandLoggerEventHandler implements BeforeEventHandler {
    private final Set<String> commandsToLog = new HashSet<String>();
    private final PrintWriter log;
    private final MessageFormat logfilePattern = new MessageFormat(":{0}:{1}:{2}:{3}");
    private final DateFormat time = new SimpleDateFormat("EEE MMM d HH:mm:ss yyyy", Locale.ENGLISH);
    private boolean logAll = false;

    /**
     *
     * @param log the log to which this data will be piped
     * @param commands commands should be in a string separated by spaces (" ") and/or commas (",")
     */
    public SiteCommandLoggerEventHandler(PrintWriter log, String commands) {
        this.log = log;
        String[] cs = commands.split("\\s+|,");
        for (String c : cs) {
            if (c != null && !"".equals(c)) {
                if ("*".equals(c)) {
                    logAll = true;
                    break; // we don't need to add any more strings
                }
                commandsToLog.add(c.toLowerCase());
            }
        }
    }

    public boolean handleEvent(Event event, Connection connection) {
        if (event.getType() == Event.SITE_COMMAND) {
            String fullCommand = event.getProperty("site.command");
            String command;
            int index = fullCommand.indexOf(' ');
            if (index > -1) {
                command = fullCommand.substring(0, index);
            } else {
                command = fullCommand;
            }
            if (logAll || commandsToLog.contains(command.toLowerCase())) {
                log.println(time.format(new Date()) + logfilePattern.format(new Object[]{event.getUser().getUsername(), connection.getConnectionId(), connection.getClientHost(), fullCommand}));
                log.flush();
            }
        }
        return true;
    }
}
