package cu.ftpd.commands.transfer;

import cu.ftpd.user.User;
import cu.shell.ProcessResult;

import java.io.File;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version $Id: TransferPostProcessor.java 258 2008-10-26 12:47:23Z jevring $
 * @since 2008-okt-03 - 13:00:44
 */
public interface TransferPostProcessor {
    
    /**
     * Implementations of this method performs integrity checks and any other post processing that is required.
     * This method takes as parameters the environment in which the file was uploaded (user, section), as well
     * as information about the file itself.
     * An example of this use might be integrity verification via comparison of the checksum of the transfer to some known 
     * checksum of the file.
     *
     * @param file the file that was uploaded
     * @param checksum the checksum of the transferred file, calculated during the upload
     * @param bytesTransferred how many bytes that were transfered
     * @param transferTime how long time the transfer took
     * @param user the user that transferred the file
     * @param section the section in which the file was transfered
     * @param transferSpeedInKBPerSecond the speed of the transfer, in kilobytes per second
     * @return The result of processing. ProcessResult.exitValue = 0 indicates success.
     */
    public ProcessResult process(File file,
                                 long checksum,
                                 long bytesTransferred,
                                 long transferTime,
                                 User user,
                                 String section,
                                 long transferSpeedInKBPerSecond);
}
