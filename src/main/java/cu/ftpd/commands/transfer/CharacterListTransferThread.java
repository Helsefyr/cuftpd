/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.ftpd.commands.transfer;

import java.util.List;
import java.io.Writer;
import java.io.IOException;
import java.net.Socket;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2008-jan-09 - 11:01:20
 * @version $Id: CharacterListTransferThread.java 258 2008-10-26 12:47:23Z jevring $
 */
public class CharacterListTransferThread extends TransferThread {
    private final List<String> list;
    private final Writer out;

    public CharacterListTransferThread(TransferController controller, List<String> list, Writer out) {
        super(controller);
        this.list = list;
        this.out = out;
    }

    public CharacterListTransferThread(TransferController controller, int bufferSize, List<String> list, Writer out) {
        super(controller, bufferSize);
        this.list = list;
        this.out = out;
    }

    public void transfer() throws IOException {
        for (String line : list) {
            out.write(line + "\r\n");
            out.flush();
            bytesTransferred += line.length() + 2;
        }
    }
}
