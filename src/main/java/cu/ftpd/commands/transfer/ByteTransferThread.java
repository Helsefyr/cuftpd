/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.ftpd.commands.transfer;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;

/**
 * This calss facilitates byte transfers between two streams. It does nothing but transfer. It doesn't close or open the streams.
 * If an error occurs, it invokes the .error() method on the creating object the the exception as a parameter.
 * When the transfer is complete, and no error has occured, it invokes the .complete() method on the creating object.
 *
 * @author Markus Jevring <markus@jevring.net>
 * @since 2008-jan-08 - 18:34:07
 * @version $Id: ByteTransferThread.java 258 2008-10-26 12:47:23Z jevring $
 */
public class ByteTransferThread extends TransferThread {
    protected byte[] buf;
    protected final InputStream in;
    protected final OutputStream out;

    public ByteTransferThread(TransferController controller, InputStream in, OutputStream out) {
        super(controller);
        this.in = in;
        this.out = out;
        this.buf = new byte[bufferSize];
    }

    public ByteTransferThread(TransferController controller, InputStream in, OutputStream out, int bufsize) {
        super(controller, bufsize);
        this.in = in;
        this.out = out;
        buf = new byte[bufsize];
    }

    public void transfer() throws IOException {
        // we make this available in case we want to to non-threaded transfers
        int len;
        while ((len = in.read(buf)) != -1) {
            out.write(buf, 0, len);
            bytesTransferred += len;
        }
    }
}
