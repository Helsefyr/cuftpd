/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.ftpd.filesystem.permissions;

import java.util.regex.Pattern;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2008-mar-10 - 20:27:37
 * @version $Id: Permission.java 258 2008-10-26 12:47:23Z jevring $
 */
public abstract class Permission {
    // rule types
    public static final int ALLOW = 1;
    public static final int DENY = 2;
    public static final int HIDE = 3;
    public static final int SHOW = 4;
    public static final int RESTRICT = 5;
    public static final int UNRESTRICT = 6;
    

    protected final String directory;
    protected final boolean quick;
    protected final Pattern directoryPattern;

    public Permission(boolean quick, String directory) {
        this.quick = quick;
        if (!directory.contains("*")) {
            // to handle backwards compatibility, we are appending a "*" to any path that does NOT already contain a wildcard.
            directory += "*";
        }
        this.directory = directory;
        this.directoryPattern = Pattern.compile(this.directory.replaceAll("\\*", ".*").replaceAll("\\?", ".?"));
    }

    public String getDirectory() {
        return directory;
    }

    public Pattern getDirectoryPattern() {
        return directoryPattern;
    }

    public boolean isQuick() {
        return quick;
    }

    public String toString() {
        return ";" + directory + ";quick:" + quick; 
    }
}
