/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.ftpd.filesystem.permissions;

import java.util.Set;
import java.util.HashSet;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2008-mar-10 - 20:31:48
 * @version $Id: UserAndGroupPermission.java 258 2008-10-26 12:47:23Z jevring $
 */
public abstract class UserAndGroupPermission extends Permission {
    protected final Set<String> users = new HashSet<String>();
    protected final Set<String> groups = new HashSet<String>();
    
    public UserAndGroupPermission(boolean quick, String directory) {
        super(quick, directory);
    }

    public boolean appliesToUser(String username) {
        return users.contains(username) || users.contains("*");
    }

    public boolean appliesToGroup(String group) {
        return groups.contains(group) || groups.contains("*");
    }

    public boolean appliesToGroups(Set<String> groups) {
        if (this.groups.contains("*")) {
            return true;
        } else {
            for (String group : groups) {
                if (this.groups.contains(group)){
                    // if the user is in ANY group for which this applies, return true
                    return true;
                }
            }
        }
        return false;
        //return groups.containsAll(groups); // how fitting that we are returning the user's groups in a usable format.
    }

    public void addGroup(String group) {
        groups.add(group);
    }

    public void addUser(String username) {
        users.add(username);
    }

    public String toString() {
        return ";g:" + groups + ";u:" + users;
    }
}
