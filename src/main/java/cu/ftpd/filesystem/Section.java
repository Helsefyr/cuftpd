/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.filesystem;

import java.io.File;
import java.io.Serializable;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-maj-22 : 22:19:23
 * @version $Id: Section.java 258 2008-10-26 12:47:23Z jevring $
 */
public class Section implements Serializable {
    private String name;
    private String path;
    private String owner;
    private String group;
    private int ratio;

    public Section(String name, String path, String owner, String group, int ratio) {
        if (name == null || path == null || owner == null || group == null) {
            throw new IllegalArgumentException("Sections cannot have unspecified parameters.");
        }
        this.name = name;
        this.path = new File(path).getAbsolutePath(); // this should resolve any issues we have with not having a "/" or "\" at the end;
        this.owner = owner;
        this.group = group;
        this.ratio = ratio;
    }


    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }

    public String getOwner() {
        return owner;
    }

    public String getGroup() {
        return group;
    }

    public int getRatio() {
        return ratio;
    }
/*
    public Section clone() {
        return new Section(this.name, this.path, this.owner, this.group, this.ratio);
    }
*/
}
