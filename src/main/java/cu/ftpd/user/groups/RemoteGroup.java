/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.user.groups;

import cu.ftpd.logging.Logging;
import cu.ftpd.user.userbases.remote.client.RemoteUserbase;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-aug-30 : 21:45:46
 * @version $Id: RemoteGroup.java 258 2008-10-26 12:47:23Z jevring $
 */
public class RemoteGroup implements Group {
    private String name;
    private final RemoteUserbase userbase;

    public RemoteGroup(String name, RemoteUserbase userbase) {
        this.name = name;
        this.userbase = userbase;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSlots() {
        try {
            return Integer.parseInt(userbase.getGroupProperty(name, "getgroupslots"));
        } catch (NumberFormatException e) {
            Logging.getErrorLog().reportError("Groupfile for group '" + name + "' might be corrupt for value slots: " + e.getMessage());
            return 0;
        }
    }

    public void setSlots(int slots) {
        userbase.setGroupProperty(name, "setgroupslots", String.valueOf(slots));
    }

    public int getLeechSlots() {
        try {
            return Integer.parseInt(userbase.getGroupProperty(name, "getleechslots"));
        } catch (NumberFormatException e) {
            Logging.getErrorLog().reportError("Groupfile for group '" + name + "' might be corrupt for value leechslots: " + e.getMessage());
            return 0;
        }            
    }

    public void setLeechSlots(int leechSlots) {
        userbase.setGroupProperty(name, "setleechslots", String.valueOf(leechSlots));
    }

    public int getAllotmentSlots() {
        try {
            return Integer.parseInt(userbase.getGroupProperty(name, "getallotmentslots"));
        } catch (NumberFormatException e) {
            Logging.getErrorLog().reportError("Groupfile for group '" + name + "' might be corrupt for value allotmentslots: " + e.getMessage());
            return 0;
        }
    }

    public void setAllotmentSlots(int allotmentSlots) {
        userbase.setGroupProperty(name, "setallotmentslots", String.valueOf(allotmentSlots));
    }

    public long getMaxAllotment() {
        try {
            return Long.parseLong(userbase.getGroupProperty(name, "getmaxallotment"));
        } catch (NumberFormatException e) {
            Logging.getErrorLog().reportError("Groupfile for group '" + name + "' might be corrupt for value maxallotment: " + e.getMessage());
            return 0;
        }

    }

    public void setMaxAllotment(long maxAllotment) {
        userbase.setGroupProperty(name, "setmaxallotment", String.valueOf(maxAllotment));
    }

    public String getDescription() {
        return userbase.getGroupProperty(name, "getdescription");
    }

    public void setDescription(String description) {
        userbase.setGroupProperty(name, "setdescription", description);
    }
}
