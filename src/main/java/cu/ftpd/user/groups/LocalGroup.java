/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.user.groups;

import cu.ftpd.user.userbases.changetracking.ChangeTracker;
import cu.ftpd.user.userbases.changetracking.GroupChange;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-aug-19 : 17:23:51
 * @version $Id: LocalGroup.java 258 2008-10-26 12:47:23Z jevring $
 */
public class LocalGroup implements Group {
    private final ChangeTracker changeTracker;
    private String name;
    private int slots;
    private int leechSlots;
    private int allotmentSlots;
    private long maxAllotment;
    private String description;

    public LocalGroup(String name, int slots, int leechSlots, int allotmentSlots, long maxAllotment, String description, ChangeTracker changeTracker) {
        this.name = name;
        this.slots = slots;
        this.leechSlots = leechSlots;
        this.allotmentSlots = allotmentSlots;
        this.maxAllotment = maxAllotment;
        this.description = description;
        this.changeTracker = changeTracker;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSlots() {
        return slots;
    }

    @Override
    public void setSlots(int slots) {
        setSlots(slots, true);
    }

    public void setSlots(int slots, boolean propagate) {
        this.slots = slots;
        if (propagate) {
            changeTracker.addChange(new GroupChange(GroupChange.Property.Slots, name, slots));
        }
    }

    public int getLeechSlots() {
        return leechSlots;
    }

    @Override
    public void setLeechSlots(int leechSlots) {
        setLeechSlots(leechSlots, true);
    }

    public void setLeechSlots(int leechSlots, boolean propagate) {
        this.leechSlots = leechSlots;
        if (propagate) {
            changeTracker.addChange(new GroupChange(GroupChange.Property.LeechSlots, name, leechSlots));
        }
    }

    public int getAllotmentSlots() {
        return allotmentSlots;
    }

    @Override
    public void setAllotmentSlots(int allotmentSlots) {
        setAllotmentSlots(allotmentSlots, true);
    }

    public void setAllotmentSlots(int allotmentSlots, boolean propagate) {
        this.allotmentSlots = allotmentSlots;
        if (propagate) {
            changeTracker.addChange(new GroupChange(GroupChange.Property.AllotmentSlots, name, allotmentSlots));
        }
    }

    public long getMaxAllotment() {
        return maxAllotment;
    }

    @Override
    public void setMaxAllotment(long maxAllotment) {
        setMaxAllotment(maxAllotment, true);
    }

    public void setMaxAllotment(long maxAllotment, boolean propagate) {
        this.maxAllotment = maxAllotment;
        if (propagate) {
            changeTracker.addChange(new GroupChange(GroupChange.Property.MaxAllotment, name, maxAllotment));
        }
    }

    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        setDescription(description, true);
    }

    public void setDescription(String description, boolean propagate) {
        this.description = description;
        if (propagate) {
            changeTracker.addChange(new GroupChange(GroupChange.Property.Description, name, description));
        }
    }

    public String toString() {
        return name + ';' + slots + ';' + leechSlots + ';' + allotmentSlots + ';' + maxAllotment + ';' + description;
    }
}
