package cu.ftpd.user.userbases.changetracking;

import cu.ftpd.logging.Logging;
import cu.ftpd.user.User;
import cu.ftpd.user.groups.GroupExistsException;
import cu.ftpd.user.groups.LocalGroup;
import cu.ftpd.user.groups.NoSuchGroupException;
import cu.ftpd.user.userbases.NoSuchUserException;
import cu.ftpd.user.userbases.UserExistsException;
import cu.ftpd.user.userbases.local.LocalUser;
import cu.ftpd.user.userbases.local.LocalUserbase;

import java.util.HashMap;
import java.util.Map;

/**
 * @author markus@jevring.net
 */
public class ChangeApplicator {
    private static final Map<UserChange.Property, UserChangeApplicator> userChangeActions = new HashMap<>();
    private static final Map<UserbaseChange.Property, UserbaseChangeApplicator> userbaseChangeActions = new HashMap<>();
    private static final Map<GroupChange.Property, GroupChangeApplicator> groupChangeActions = new HashMap<>();
    private final LocalUserbase userbase;

    // populate the user actions
    static {
        // string setters
        userChangeActions.put(UserChange.Property.Tagline, new UserChangeApplicator() {
            @Override
            public void apply(UserChange change, LocalUser user) {
                user.setTagline(change.getValue(), false);
            }
        });
        userChangeActions.put(UserChange.Property.PrimaryGroup, new UserChangeApplicator() {
            @Override
            public void apply(UserChange change, LocalUser user) {
                user.setPrimaryGroup(change.getValue(), false);
            }
        });
        userChangeActions.put(UserChange.Property.HashedPassword, new UserChangeApplicator() {
            @Override
            public void apply(UserChange change, LocalUser user) {
                user.passwd(change.getValue(), false);
            }
        });
        // boolean setters
        userChangeActions.put(UserChange.Property.Hidden, new UserChangeApplicator() {
            @Override
            public void apply(UserChange change, LocalUser user) {
                user.setHidden(Boolean.parseBoolean(change.getValue()), false);
            }
        });
        userChangeActions.put(UserChange.Property.Leech, new UserChangeApplicator() {
            @Override
            public void apply(UserChange change, LocalUser user) {
                user.setLeech(Boolean.parseBoolean(change.getValue()), false);
            }
        });
        userChangeActions.put(UserChange.Property.Suspended, new UserChangeApplicator() {
            @Override
            public void apply(UserChange change, LocalUser user) {
                user.setSuspended(Boolean.parseBoolean(change.getValue()), false);
            }
        });
        // number setters
        userChangeActions.put(UserChange.Property.LastLog, new UserChangeApplicator() {
            @Override
            public void apply(UserChange change, LocalUser user) {
                user.setLastlog(Long.parseLong(change.getValue()), false);
            }
        });
        userChangeActions.put(UserChange.Property.Logins, new UserChangeApplicator() {
            @Override
            public void apply(UserChange change, LocalUser user) {
                user.setLogins(Integer.parseInt(change.getValue()), false);
            }
        });
        // complex setters
        userChangeActions.put(UserChange.Property.Group, new UserChangeApplicator() {
            @Override
            public void apply(UserChange change, LocalUser user) {
                switch (change.getModification()) {
                    case Add:
                        user.addGroup(change.getValue(), false);
                        break;
                    case Remove:
                        user.removeGroup(change.getValue(), false);
                        break;
                }
            }
        });
        userChangeActions.put(UserChange.Property.Credits, new UserChangeApplicator() {
            @Override
            public void apply(UserChange change, LocalUser user) {
                switch (change.getModification()) {
                    case Add:
                        user.giveCredits(Long.parseLong(change.getValue()), false);
                        break;
                    case Remove:
                        user.takeCredits(Long.parseLong(change.getValue()), false);
                        break;
                    case Set:
                        user.setCredits(Long.parseLong(change.getValue()), false);
                        break;
                }
            }
        });
        userChangeActions.put(UserChange.Property.GroupAdmin, new UserChangeApplicator() {
            @Override
            public void apply(UserChange change, LocalUser user) {
                switch (change.getModification()) {
                    case Add:
                        user.addGadminForGroup(change.getValue(), false);
                        break;
                    case Remove:
                        user.removeGadminForGroup(change.getValue(), false);
                        break;
                }
            }
        });
        userChangeActions.put(UserChange.Property.Permission, new UserChangeApplicator() {
            @Override
            public void apply(UserChange change, LocalUser user) {
                switch (change.getModification()) {
                    case Add:
                        user.addPermission(Integer.parseInt(change.getValue()), false);
                        break;
                    case Remove:
                        user.delPermission(Integer.parseInt(change.getValue()), false);
                        break;
                }
            }
        });
        userChangeActions.put(UserChange.Property.Ip, new UserChangeApplicator() {
            @Override
            public void apply(UserChange change, LocalUser user) {
                switch (change.getModification()) {
                    case Add:
                        user.addIp(change.getValue(), false);
                        break;
                    case Remove:
                        user.delIp(change.getValue(), false);
                        break;
                }
            }
        });
    }

    // populate the userbase actions
    static {
        userbaseChangeActions.put(UserbaseChange.Property.Group, new UserbaseChangeApplicator() {
            @Override
            public void apply(UserbaseChange change, LocalUserbase userbase) {
                if (change.getModification() == Modification.Add) {
                    try {
                        userbase.createGroup(change.getParameters()[0], change.getParameters()[1], false);
                    } catch (GroupExistsException e) {
                        // this can happen if the group was deleted on a peer, and was deleted here at the same time,
                        // but the change hasn't had time to propagate yet
                        Logging.getErrorLog().reportError("Group '" + change.getParameters()[0] + "' already exists. Asynchronous change disregarded. Was: " + change);
                    }
                } else if (change.getModification() == Modification.Add) {
                    try {
                        userbase.deleteGroup(change.getParameters()[0], false);
                    } catch (NoSuchGroupException e) {
                        // this can happen if the group was deleted on a peer, and was deleted here at the same time,
                        // but the change hasn't had time to propagate yet
                        Logging.getErrorLog().reportError("Group '" + change.getParameters()[0] + "' doesn't exists. Asynchronous change disregarded. Was: " + change);
                    }
                }

            }
        });
        userbaseChangeActions.put(UserbaseChange.Property.User, new UserbaseChangeApplicator() {
            @Override
            public void apply(UserbaseChange change, LocalUserbase userbase) {
                if (change.getModification() == Modification.Add) {
                    try {
                        User user = userbase.addUser(change.getParameters()[0], change.getParameters()[1], false);
                        for (int i = 3; i < change.getParameters().length; i++) {
                            user.addIp(change.getParameters()[i]);
                        }
                    } catch (UserExistsException e) {
                        // this can happen if the user was added on a peer, and was added here at the same time,
                        // but the change hasn't had time to propagate yet
                        Logging.getErrorLog().reportError("User '" + change.getParameters()[0] + "' already exists. Asynchronous change disregarded. Was: " + change);
                    }
                } else if (change.getModification() == Modification.Remove) {
                    try {
                        userbase.deleteUser(change.getParameters()[0], false);
                    } catch (NoSuchUserException e) {
                        // this can happen if the group was deleted on a peer, and was deleted here at the same time,
                        // but the change hasn't had time to propagate yet
                        Logging.getErrorLog().reportError("User '" + change.getParameters()[0] + "' doesn't exists. Asynchronous change disregarded. Was: " + change);
                    }
                }
            }
        });
    }

    // populate the group actions
    static {
        groupChangeActions.put(GroupChange.Property.Slots, new GroupChangeApplicator() {
            @Override
            public void apply(GroupChange change, LocalGroup group) {
                group.setSlots(Integer.parseInt(change.getParameter()), false);
            }
        });
        groupChangeActions.put(GroupChange.Property.AllotmentSlots, new GroupChangeApplicator() {
            @Override
            public void apply(GroupChange change, LocalGroup group) {
                group.setAllotmentSlots(Integer.parseInt(change.getParameter()), false);
            }
        });
        groupChangeActions.put(GroupChange.Property.Description, new GroupChangeApplicator() {
            @Override
            public void apply(GroupChange change, LocalGroup group) {
                group.setDescription(change.getParameter(), false);
            }
        });
        groupChangeActions.put(GroupChange.Property.LeechSlots, new GroupChangeApplicator() {
            @Override
            public void apply(GroupChange change, LocalGroup group) {
                group.setLeechSlots(Integer.parseInt(change.getParameter()), false);
            }
        });
        groupChangeActions.put(GroupChange.Property.MaxAllotment, new GroupChangeApplicator() {
            @Override
            public void apply(GroupChange change, LocalGroup group) {
                group.setMaxAllotment(Integer.parseInt(change.getParameter()), false);
            }
        });
    }

    public ChangeApplicator(LocalUserbase userbase) {
        this.userbase = userbase;
    }

    public void apply(Change change) {
        if (change instanceof UserChange) {
            UserChange userChange = (UserChange) change;
            try {
                LocalUser user = userbase.getUser(userChange.getUsername());
                userChangeActions.get(userChange.getProperty()).apply(userChange, user);
            } catch (NoSuchUserException e) {
                Logging.getErrorLog().reportError("User '" + userChange.getUsername() + "' doesn't exists. Asynchronous change disregarded. Was: " + change);
            }
        } else if (change instanceof UserbaseChange) {
            UserbaseChange userbaseChange = (UserbaseChange) change;
            userbaseChangeActions.get(userbaseChange.getProperty()).apply(userbaseChange, userbase);
        } else if (change instanceof GroupChange) {
            GroupChange groupChange = (GroupChange) change;
            try {
                LocalGroup group = userbase.getGroup(groupChange.getGroup());
                groupChangeActions.get(groupChange.getProperty()).apply(groupChange, group);
            } catch (NoSuchGroupException e) {
                Logging.getErrorLog().reportError("Group '" + groupChange.getGroup() + "' doesn't exists. Asynchronous change disregarded. Was: " + change);
            }

        }

    }

    private static interface UserChangeApplicator {
        void apply(UserChange change, LocalUser user);
    }

    private static interface UserbaseChangeApplicator {
        void apply(UserbaseChange change, LocalUserbase userbase);
    }

    private static interface GroupChangeApplicator {
        void apply(GroupChange change, LocalGroup group);
    }
}
