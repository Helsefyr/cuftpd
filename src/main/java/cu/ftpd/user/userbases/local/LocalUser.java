/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.user.userbases.local;

import cu.ftpd.logging.Logging;
import cu.ftpd.user.User;
import cu.ftpd.user.UserPermission;
import cu.ftpd.user.userbases.changetracking.*;

import java.io.*;
import java.util.*;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-maj-13 : 23:31:45
 * @version $Id: LocalUser.java 258 2008-10-26 12:47:23Z jevring $
 */
public class LocalUser implements User {
    // note: this class implements Serializable, because some times it gets transferred over the wire as read-only, and htne it is convenient to transfer the whole thing
    private final Object creditLock = new Object();
    private final String username;
    private long credits;
    private long created;
    private long lastlog;
    private int logins = 2;
    private boolean leech = false;
    private boolean suspended;
    private boolean hidden;
    private String hashedPassword;
    private String tagline = "No tagline set";
    private String primaryGroup = "";
    //private String permissions = "";
    private final List<String> ips;
    private final Set<String> gadminGroups;
    private final Set<String> groups;
    private final Set<Integer> permissions;
    private final LocalUserbase userbase;
    private final ChangeTracker changeTracker;

    public LocalUser(String username, String hashedPassword, LocalUserbase userbase, ChangeTracker changeTracker) {
        this.username = username;
        this.hashedPassword = hashedPassword;
        this.changeTracker = changeTracker;
        this.ips = new ArrayList<>();
        this.groups = new HashSet<>();
        this.gadminGroups = new HashSet<>();
        this.permissions = new TreeSet<>();
        this.userbase = userbase;
        // note that this is overridden by load() when we load the user from disk
        this.created = System.currentTimeMillis();
        this.permissions.add(UserPermission.WHO);
        this.permissions.add(UserPermission.UPTIME);
        this.permissions.add(UserPermission.TRAFFIC);
        this.permissions.add(UserPermission.SEEN);
        this.permissions.add(UserPermission.STATISTICS);
    }
    
    @Override
    public void setTagline(String tagline) {
        setTagline(tagline, true);
    }
    public void setTagline(String tagline, boolean propagate) {
        this.tagline = tagline;
        userbase.store(this);
        if (propagate) {
            changeTracker.addChange(new UserChange(username, Modification.Set, UserChange.Property.Tagline, tagline));
        }
    }
    @Override
    public void setSuspended(boolean suspended) {
        setSuspended(suspended, true);
    }
    public void setSuspended(boolean suspended, boolean propagate) {
        this.suspended = suspended;
        userbase.store(this);
        if (propagate) {
            changeTracker.addChange(new UserChange(username, Modification.Set, UserChange.Property.Suspended, suspended));
        }
    }
    @Override
    public void setLogins(int logins) {
        setLogins(logins, true);
    }
    public void setLogins(int logins, boolean propagate) {
        this.logins = logins;
        userbase.store(this);
        if (propagate) {
            changeTracker.addChange(new UserChange(username, Modification.Set, UserChange.Property.Logins, logins));
        }
    }
    @Override
    public void setLeech(boolean hasLeech) {
        setLeech(hasLeech, true);
    }
    public void setLeech(boolean hasLeech, boolean propagate) {
        this.leech = hasLeech;
        userbase.store(this);
        if (propagate) {
            changeTracker.addChange(new UserChange(username, Modification.Set, UserChange.Property.Leech, true));
        }
    }
    @Override
    public void setLastlog(long lastlog) {
        setLastlog(lastlog, true);
    }
    public void setLastlog(long lastlog, boolean propagate) {
        this.lastlog = lastlog;
        userbase.store(this);
        if (propagate) {
            changeTracker.addChange(new UserChange(username, Modification.Set, UserChange.Property.LastLog, lastlog));
        }
    }
    @Override
    public void setHidden(boolean hidden) {
        setHidden(hidden, true);
    }
    public void setHidden(boolean hidden, boolean propagate) {
        this.hidden = hidden;
        userbase.store(this);
        if (propagate) {
            changeTracker.addChange(new UserChange(username, Modification.Set, UserChange.Property.Hidden, hidden));
        }
    }

    public String getHashedPassword() {
        return hashedPassword;
    }
    public String getUsername() {
        return username;
    }
    public String getTagline() {
        return tagline;
    }
    public int getLogins() {
        return logins;
    }
    public boolean hasLeech() {
        return leech;
    }
    public long getCreated() {
        return created;
    }
    public long getLastlog() {
        return lastlog;
    }
    public boolean isHidden() {
        return hidden;
    }
    public boolean isSuspended() {
        return suspended;
    }

    // credits
    @Override
    public void setCredits(long credits) {
        setCredits(credits, true);
    }
    public void setCredits(long credits, boolean propagate) {
        synchronized(creditLock) {
            this.credits = credits;
            userbase.store(this);
            if (propagate) {
                changeTracker.addChange(new UserChange(username, Modification.Set, UserChange.Property.Credits, credits));
            }
        }
    }
    @Override
    public void takeCredits(long credits) {
        takeCredits(credits, true);
    }
    public void takeCredits(long credits, boolean propagate) {
        synchronized(creditLock) {
            this.credits -= credits;
            userbase.store(this);
            if (propagate) {
                changeTracker.addChange(new UserChange(username, Modification.Remove, UserChange.Property.Credits, credits));
            }
        }
    }
    @Override
    public void giveCredits(long credits) {
        giveCredits(credits, true);
    }
    public void giveCredits(long credits, boolean propagate) {
        synchronized(creditLock) {
            this.credits += credits;
            userbase.store(this);
            if (propagate) {
                changeTracker.addChange(new UserChange(username, Modification.Add, UserChange.Property.Credits, credits));
            }
        }
    }
    public long getCredits() {
        synchronized(creditLock) {
            return credits;
        }
    }

    // ips:
    @Override
    public void addIp(String ip) {
        addIp(ip, true);
    }
    public void addIp(String ip, boolean propagate) {
        ips.add(ip);
        userbase.store(this);
        if (propagate) {
            changeTracker.addChange(new UserChange(username, Modification.Add, UserChange.Property.Ip, ip));
        }
    }
    @Override
    public void delIp(String pattern) {
        delIp(pattern, true);
    }
    public void delIp(String pattern, boolean propagate) {
        ips.remove(pattern);
        userbase.store(this);
        if (propagate) {
            changeTracker.addChange(new UserChange(username, Modification.Remove, UserChange.Property.Ip, pattern));
        }
    }
    public Collection<String> getIps() {
        return ips;
    }

    // permissions:
    @Override
    public void addPermission(int permission) {
        addPermission(permission, true);
    }
    public void addPermission(int permission, boolean propagate) {
        permissions.add(permission);
        userbase.store(this);
        if (propagate) {
            changeTracker.addChange(new UserChange(username, Modification.Add, UserChange.Property.Permission, permission));
        }
    }
    @Override
    public void delPermission(int permission) {
        delPermission(permission, true);
    }
    public void delPermission(int permission, boolean propagate) {
        permissions.remove(permission);
        userbase.store(this);
        if (propagate) {
            changeTracker.addChange(new UserChange(username, Modification.Remove, UserChange.Property.Permission, permission));
        }
    }
    public String getPermissions() {
        return permissions.toString().replace("[", "").replace("]","").replace(" ", "");
    }
    public boolean hasPermission(int permission) {
        return permissions.contains(permission);
    }

    // gadmin:
    @Override
    public void addGadminForGroup(String group) {
        addGadminForGroup(group, true);
    }
    public void addGadminForGroup(String group, boolean propagate) {
        gadminGroups.add(group);
        userbase.store(this);
        if (propagate) {
            changeTracker.addChange(new UserChange(username, Modification.Add, UserChange.Property.GroupAdmin, group));
        }
    }
    @Override
    public void removeGadminForGroup(String group) {
        removeGadminForGroup(group, true);
    }
    public void removeGadminForGroup(String group, boolean propagate) {
        gadminGroups.remove(group);
        userbase.store(this);
        if (propagate) {
            changeTracker.addChange(new UserChange(username, Modification.Remove, UserChange.Property.GroupAdmin, group));
        }
    }
    public Set<String> getGadminGroups() {
        return gadminGroups;
    }
    public boolean isGadminForGroup(String group) {
        return gadminGroups.contains(group);
    }

    // groups:
    @Override
    public void addGroup(String group) {
        addGroup(group, true);
    }
    public void addGroup(String group, boolean propagate) {
        groups.add(group);
        userbase.store(this);
        if (propagate) {
            changeTracker.addChange(new UserChange(username, Modification.Add, UserChange.Property.Group, group));
        }
    }
    @Override
    public void removeGroup(String group) {
        removeGroup(group, true);
    }
    public void removeGroup(String group, boolean propagate) {
        groups.remove(group);
        if (primaryGroup.equalsIgnoreCase(group)) {
            primaryGroup = "";
            if (propagate) {
                changeTracker.addChange(new UserChange(username, Modification.Set, UserChange.Property.PrimaryGroup, ""));
            }
        }
        userbase.store(this);
        if (propagate) {
            changeTracker.addChange(new UserChange(username, Modification.Remove, UserChange.Property.Group, permissions));
        }
    }
    public boolean isMemberOfGroup(String group) {
        return groups.contains(group);
    }
    public Set<String> getGroups() {
        return groups;
    }
    public String getPrimaryGroup() {
        if (primaryGroup == null || "".equals(primaryGroup)) {
            return "default";
        } else {
            return primaryGroup;
        }
    }

    @Override
    public void setPrimaryGroup(String primaryGroup) {
        setPrimaryGroup(primaryGroup, true);
    }
    public void setPrimaryGroup(String primaryGroup, boolean propagate) {
        this.primaryGroup = primaryGroup;
        userbase.store(this);
        if (propagate) {
            changeTracker.addChange(new UserChange(username, Modification.Set, UserChange.Property.PrimaryGroup, primaryGroup));
        }
    }

    @Override
    public void passwd(String hashedPassword) {
        passwd(hashedPassword, true);
    }
    public void passwd(String hashedPassword, boolean propagate) {
        this.hashedPassword = hashedPassword;
        userbase.store(this);
        if (propagate) {
            changeTracker.addChange(new UserChange(username, Modification.Set, UserChange.Property.HashedPassword, hashedPassword));
        }
    }

    public String toString() {
        StringBuilder data = new StringBuilder(500); // make it big enough so that we don't have to resize it all the time
        data.append("username=").append(username).append("\r\n");
        data.append("password=").append(hashedPassword).append("\r\n");
        data.append("credits=").append(getCredits()).append("\r\n");
        data.append("created=").append(created).append("\r\n");
        data.append("tagline=").append(tagline).append("\r\n");
        data.append("suspended=").append(suspended).append("\r\n");
        data.append("logins=").append(logins).append("\r\n");
        data.append("permissions=").append(getPermissions()).append("\r\n");
        data.append("primarygroup=").append(primaryGroup).append("\r\n");
        data.append("lastlog=").append(lastlog).append("\r\n");
        data.append("hidden=").append(hidden).append("\r\n");
        for (String ip : ips) {
            data.append("ip=").append(ip).append("\r\n");
        }
        for (String group : groups) {
            data.append("group=").append(group).append("\r\n");
        }
        for (String group : gadminGroups) {
            data.append("gadmin=").append(group).append("\r\n");
        }
        return data.toString();
    }

    void fill(File userfile) {
        // it would be cool if we could get something like a HashMap in here with the values, but that's only if we want to be able to extend it, in which case we'll use the User interface
        // fills the user object with all the data
        //long start = System.currentTimeMillis();
        BufferedReader in = null;
        String line;
        this.permissions.clear();
        try {
            // NOTE: don't move this to the user object, since the user object doesn't know about the userdir
            in = new BufferedReader(new InputStreamReader(new FileInputStream(userfile)));
            while((line = in.readLine()) != null) {
                try {
                    String[] lp = line.split("=", 2);
                    if ("password".equalsIgnoreCase(lp[0])) {
                        // _todo: disallow empty passwords
                        // no problem. someone with an empty password just can't log in.
                        hashedPassword = lp[1];
                    } else if ("ip".equalsIgnoreCase(lp[0])) {
                        ips.add(lp[1]);
                    } else if ("group".equalsIgnoreCase(lp[0])) {
                        groups.add(lp[1]);
                    } else if ("gadmin".equalsIgnoreCase(lp[0])) {
                        gadminGroups.add(lp[1]);
                    } else if ("credits".equalsIgnoreCase(lp[0])) {
                        credits = Long.parseLong(lp[1]);
                    } else if ("created".equalsIgnoreCase(lp[0])) {
                        created = Long.parseLong(lp[1]);
                    } else if ("tagline".equalsIgnoreCase(lp[0])) {
                        tagline = lp[1];
                    } else if ("suspended".equalsIgnoreCase(lp[0])) {
                        suspended = Boolean.parseBoolean(lp[1]);
                    } else if ("logins".equalsIgnoreCase(lp[0])) {
                        logins = Integer.parseInt(lp[1]);
                    } else if ("permissions".equalsIgnoreCase(lp[0])) {
                        String[] s = lp[1].split("\\s?,\\s?");
                        for (String value : s) {
                            if (value.length() > 0) {
                                try {
                                    permissions.add(Integer.parseInt(value));
                                } catch (NumberFormatException e) {
                                    Logging.getErrorLog().reportCritical("permission " + value + " in userfile " + userfile.getName() + " is not a number, skipping.");
                                }
                            }
                        }
                    } else if ("primarygroup".equalsIgnoreCase(lp[0])) {
                        primaryGroup = lp[1];
                    } else if ("lastlog".equalsIgnoreCase(lp[0])) {
                        lastlog = Long.parseLong(lp[1]);
                    } else if ("hidden".equalsIgnoreCase(lp[0])) {
                        hidden = Boolean.parseBoolean(lp[1]);
                    }
                } catch (NumberFormatException e) {
                    Logging.getErrorLog().reportError("Parsing of userfile line for user '" + userfile.getName() + "' failed because something that was supposed to be a number wasn't: " + e.getMessage() + '(' + line + ')');
                }
            }
            leech = userbase.hasLeech(username);
            if (!groups.contains(primaryGroup)) {
                // this can happen if somebody edited the userfile offline
                primaryGroup = "";
            }
        } catch (IOException e) {
            Logging.getErrorLog().reportError("Failed to read user from disk: " + e.getMessage());
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        //System.out.println((System.currentTimeMillis() - start) + " milliseconds taken to read userfile " + username);
    }
}
