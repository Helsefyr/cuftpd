/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.user.userbases.actions.formatters;

import cu.ftpd.logging.Formatter;
import cu.ftpd.logging.Logging;
import cu.ftpd.user.User;
import cu.ftpd.user.statistics.StatisticsEntry;
import cu.ftpd.user.statistics.UserStatistics;
import cu.ftpd.Server;
import cu.ftpd.ServiceManager;

import java.io.*;
import java.util.ArrayList;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-okt-01 : 16:37:34
 * @version $Id: UserFormatter.java 258 2008-10-26 12:47:23Z jevring $
 */
public class UserFormatter {
    private static final ArrayList<String> format = new ArrayList<String>();

    public UserFormatter(File formatterTemplateFile) {
        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(new FileInputStream(formatterTemplateFile),"ISO-8859-1"));
            String line;
            while ((line = in.readLine()) != null) {
                format.add(line);
            }
        } catch (IOException e) {
            Logging.getErrorLog().reportError("Error reading user template file: " + e.getClass() + ":" + e.getMessage());
            format.clear(); // this causes the default format to be used
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String format(User user) {
        if (format.isEmpty()) {
            return getDefaultFormat(user);
        } else {
            return getTemplateFormat(user);
        }
    }


    private String getTemplateFormat(User user) {
        // don't show allotments here, let them list allotments for that (then maybe we should be able to select on user there then, otherwise there'll be A LOT of allotments)
        StringBuilder sb = new StringBuilder(2000);
        sb.append(format.get(0)).append("\r\n");
        sb.append(String.format(format.get(1), user.getUsername())).append("\r\n");
        sb.append(String.format(format.get(2), user.getPrimaryGroup())).append("\r\n");
        sb.append(String.format(format.get(3), Formatter.size(user.getCredits()))).append("\r\n");
        sb.append(String.format(format.get(4), Formatter.datetime(user.getCreated()))).append("\r\n");
        sb.append(String.format(format.get(5), user.isHidden())).append("\r\n");
        sb.append(String.format(format.get(6), user.isSuspended())).append("\r\n");
        sb.append(String.format(format.get(7), (user.getLastlog() > 0 ? Formatter.datetime(user.getLastlog()) : "Never"))).append("\r\n");
        sb.append(String.format(format.get(8), user.getLogins())).append("\r\n");
        sb.append(String.format(format.get(9), user.getTagline())).append("\r\n");
        sb.append(String.format(format.get(10), user.getPermissions())).append("\r\n");
        sb.append(String.format(format.get(11), user.hasLeech())).append("\r\n");
        sb.append(format.get(12)).append("\r\n");
        sb.append(format.get(13)).append("\r\n");
        sb.append(format.get(14)).append("\r\n");
        // stats
        StatisticsEntry se = ServiceManager.getServices().getUserStatistics().getUserStatistics(user.getUsername(), "default");
        sb.append(String.format(format.get(15), se.get(UserStatistics.ALLUP_FILES), se.get(UserStatistics.ALLDN_FILES))).append("\r\n");
        sb.append(String.format(format.get(16), Formatter.size(se.get(UserStatistics.ALLUP_BYTES)), Formatter.size(se.get(UserStatistics.ALLDN_BYTES)))).append("\r\n");
        sb.append(String.format(format.get(17), Formatter.speed(se.get(UserStatistics.ALLUP_BYTES), se.get(UserStatistics.ALLUP_TIME)), Formatter.speed(se.get(UserStatistics.ALLDN_BYTES), se.get(UserStatistics.ALLDN_TIME)))).append("\r\n");
        
        sb.append(format.get(18)).append("\r\n");
        sb.append(format.get(19)).append("\r\n");
        sb.append(format.get(20)).append("\r\n");
        // groups
        for (String group : user.getGroups()) {
            sb.append(String.format(format.get(21), group, user.isGadminForGroup(group))).append("\r\n");
        }
        sb.append(format.get(22)).append("\r\n");
        sb.append(format.get(23)).append("\r\n");
        sb.append(format.get(24)).append("\r\n");
        // ips
        for (String ip : user.getIps()) {
            sb.append(String.format(format.get(25), ip)).append("\r\n");
        }
        sb.append(format.get(26)).append("\r\n");
        return sb.toString();
    }

    private String getDefaultFormat(User user) {
        return "200 " + user.getUsername();
    }
}
