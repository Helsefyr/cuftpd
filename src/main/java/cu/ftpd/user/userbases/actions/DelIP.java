/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.user.userbases.actions;

import cu.ftpd.Connection;
import cu.ftpd.Server;
import cu.ftpd.ServiceManager;
import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.user.User;
import cu.ftpd.user.UserPermission;
import cu.ftpd.user.userbases.NoSuchUserException;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-okt-26 : 23:58:19
 * @version $Id: DelIP.java 258 2008-10-26 12:47:23Z jevring $
 */
public class DelIP extends UserbaseAction {
    public DelIP() {
        super("delip");
    }

    public void execute(String[] parameterList, Connection connection, User user, FileSystem fs) {
        User u;
        if (parameterList.length >= 3) {
            if (!user.hasPermission(UserPermission.USEREDIT) && !currentUserIsGadminForUser(parameterList[1], user)) {
                connection.respond("531 Permission denied.");
                return;
            } else {
                try {
                    u = ServiceManager.getServices().getUserbase().getUser(parameterList[1]); // this means that we are doing the user lookup twice, but it doesn't really matter, since it's O(1) in a hash map, and this happens rarely
                } catch (NoSuchUserException e) {
                    connection.respond("500 " + e.getMessage());
                    return;
                }
            }
        } else if (parameterList.length == 2) {
            u = user;
        } else {
            connection.respond("500 Syntax error: site delip [username] ident@ip");
            return;
        }
        String malformedIps = "";
        String removedIps = "";
        String invalidIps = "";
        for (int i = 2; i < parameterList.length; i++) {
            if (ip.matcher(parameterList[i]).matches()) {
                if (u.getIps().contains(parameterList[i])) {
                    u.delIp(parameterList[i]);
                    removedIps += parameterList[i] + ' ';
                } else {
                    invalidIps += parameterList[i] + ' ';
                }
            } else {
                malformedIps += parameterList[i] + ' ';
            }
        }
        if (removedIps.length() > 0) {
            connection.respond("200 Patterns removed: " + removedIps + (invalidIps.length() > 0 ? " Patterns that the user didn't have: " + invalidIps : "") + (malformedIps.length() > 0 ? " Malformed patterns: " + malformedIps : ""));
        } else {
            connection.respond("500 No patterns removed: " + (invalidIps.length() > 0 ? " Patterns that the user didn't have: " + invalidIps : "") + (malformedIps.length() > 0 ? " Malformed patterns: " + malformedIps : ""));
        }
    }

    protected boolean currentUserIsGadminForUser(String username, User user) {
        try {
            User u = ServiceManager.getServices().getUserbase().getUser(username);
            //current user is gadmin for any of the groups the specified user is a member of
            for (String gadminGroup : user.getGadminGroups()) { // this is generally a smaller set, so it makes sense to go over that first, since we abort early
                if (u.isMemberOfGroup(gadminGroup)) {
                    return true;
                }
            }
        } catch (NoSuchUserException e) {
            // you can't be the gadmin of a user that doesn't exist
        }
        return false;
    }
}
