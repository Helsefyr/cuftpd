/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

/**
 * Copyright (c) 2005, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.ftpd.user.userbases;

/**
 * This is a simple converter for hexadecimal strings.
 *
 * @author Markus Jevring <markus@jevring.net>
 * @since 2005-06-03
 * @version $Id: Hex.java 258 2008-10-26 12:47:23Z jevring $
 *
 */
public class Hex {
    public static byte[] hexToBytes(String s) {
        char[] chars = s.toCharArray();
        byte[] out = new byte[chars.length / 2];
        // since 256 > 16, and we need to get the entire ascii range, we use to hex chars for each ascii char
        // therefore, we need to calculate the values of the chars. not their ascii value, mind you, but their hex value
        // a == 10 and such
        for (int i = 0; i < chars.length; i += 2) {
            //int t = (Character.digit(chars[i], 16) * 16) +  Character.digit(chars[i+1], 16);
            int j = Character.digit(chars[i], 16) << 4;
            j = j | Character.digit(chars[i+1], 16);

            out[(i / 2)] = (byte) (j & 0xff);
        }
        return out;
    }

    public static char[] bytesToHex(byte[] bytes) {
        char[] out = new char[bytes.length * 2];
        int k = 0;
        for (int i = 0; i < bytes.length; i++) {
            byte b = bytes[i];
            int j = (int)b;
            out[k++] = Integer.toHexString((0xf0 & j) >>> 4).charAt(0);
            out[k++] = Integer.toHexString(0x0f & j).charAt(0);
        }
        return out;
    }


    /*
      private static String bytesToHex(byte[] b) {
		StringBuffer sb = new StringBuffer (b.length * 3);
		for (int i = 0; i < b.length; ++i) {
			sb.append (Integer.toHexString ((b[i] & 0xFF) | 0x100).substring(1,3));
			sb.append (" ");
		}
		return sb.toString();
	}
    */

}
