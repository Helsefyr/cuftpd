/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.logging;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @author Markus Jevring
 * @since 2007-maj-11 : 21:19:04
 * @version $Id: EventLog.java 280 2008-11-24 18:52:18Z jevring $
 */
public class EventLog {
    // someone wanting to override this look can just subclass it (maybe have a setting for that then)
    private final DateFormat time = new SimpleDateFormat("EEE MMM d HH:mm:ss yyyy ", Locale.ENGLISH);

    private PrintWriter log;
    // receives events. writes them to the log, and possibly triggers connected event handlers

    public EventLog(String log) throws IOException {
        // We can tell users to link the log names to /dev/null instead if they want. however, this causes problems on windows, since there is no /dev/null
        try {
	        final File file = new File(log);
	        file.getParentFile().mkdirs();
	        this.log = new PrintWriter(new FileOutputStream(file, true));
        } catch (FileNotFoundException e) {
            throw new IOException("Could not open event log for writing", e);
        }
    }

    protected EventLog() {
    }

    public synchronized void log(String message) {
        log.println(time.format(new Date()) + message);
        log.flush();
    }

    public void shutdown() {
        log.close();
    }
}
