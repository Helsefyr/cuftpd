/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.ftpd.events;

import cu.ftpd.Connection;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2008-jan-25 - 21:32:19
 * @version $Id: EventHandler.java 258 2008-10-26 12:47:23Z jevring $
 */
public class EventHandler {
    private Map<Integer, List<BeforeEventHandler>> before = new HashMap<Integer, List<BeforeEventHandler>>();
    private Map<Integer, List<AfterEventHandler>> after = new HashMap<Integer, List<AfterEventHandler>>();

    public void addBeforeEventHandler(int event, BeforeEventHandler eventHandler) {
        List<BeforeEventHandler> list = before.get(event);
        if (list == null) {
            list = new LinkedList<BeforeEventHandler>();
            before.put(event, list);
        }
        list.add(eventHandler);
    }

    public void addAfterEventHandler(int event, AfterEventHandler eventHandler) {
        List<AfterEventHandler> list = after.get(event);
        if (list == null) {
            list = new LinkedList<AfterEventHandler>();
            after.put(event, list);
        }
        list.add(eventHandler);
    }

    public boolean handleBeforeEvent(Event event, Connection connection) {
        boolean ok = true;
        List<BeforeEventHandler> list = before.get(event.getType());
        if (list != null) {
            for (BeforeEventHandler eventHandler : list) {
                ok = eventHandler.handleEvent(event, connection);
                if (!ok) {
                    break;
                }
            }
        }
        return ok; // default, so if we have nothing we can just return that
    }

    public void handleAfterEvent(Event event) {
        List<AfterEventHandler> list = after.get(event.getType());
        if (list != null) {
            for (AfterEventHandler eventHandler : list) {
                eventHandler.handleEvent(event);
            }
        }
    }
}
