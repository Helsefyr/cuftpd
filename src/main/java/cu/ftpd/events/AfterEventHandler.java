package cu.ftpd.events;

/**
 * Implement this interface to handle "after" events.
 *
 * @author Markus Jevring <markus@jevring.net>
 * @since 2008-jan-25 - 17:41:52
 * @version $Id: AfterEventHandler.java 258 2008-10-26 12:47:23Z jevring $
 */
public interface AfterEventHandler {
    /**
     * This method is invoked after an event is executed in the ftpd.<br>
     * These event handlers need to be registered with the central event handler
     * to be runnable. See data/cuftpd.xml
     *
     * @param event the event containing the information about what happened.
     */
    public void handleEvent(Event event);
}
