package cu.authentication;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-nov-23 : 21:52:19
 * @version $Id: Authenticator.java 258 2008-10-26 12:47:23Z jevring $
 */
public interface Authenticator {
    /**
     * Authenticates this request against some database of users, or according to some algorithm.
     * @param auth the AuthenticationRequest to authenticate
     * @return 0 if the authentication was OK, any other number represents failure. The meaning of this number is dependent on the context in which is was used, i.e. up to the rest of the framework.
     */
    public int authenticate(AuthenticationRequest auth);
}
