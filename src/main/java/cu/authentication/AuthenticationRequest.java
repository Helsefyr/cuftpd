/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.authentication;

import java.io.Serializable;
import java.net.InetAddress;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-nov-23 : 21:47:17
 * @version $Id: AuthenticationRequest.java 258 2008-10-26 12:47:23Z jevring $
 */
public class AuthenticationRequest implements Serializable {
    private final String username;
    private String password;
    private final String ident;
    private final InetAddress host;
    private boolean authenticated = false;

    public AuthenticationRequest(String username, String ident, InetAddress host) {
        this.username = username;
        this.ident = ident;
        this.host = host;
    }

    public void setPassword(String password) {
        // Note that we can't hash the password here, as we can't possibly have access to the hash.
        // Thus, only use this over encrypted connections!
        // _todo: store the password in a char array, so that we can nullify it after the authentication has taken place
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getIdent() {
        return ident;
    }

    public InetAddress getHost() {
        return host;
    }

    public String getPassword() {
        return password;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setAuthenticated() {
        this.authenticated = true;
    }
}
