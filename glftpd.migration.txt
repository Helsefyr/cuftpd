--- How do I import my old glftpd database into cuftpd? ---

That's easy!
Because cuftpd uses the same password hashing function as glftpd, you can import your users with the same passwords
they have on your previous system.
The command you use is: java -cp cuftpd-$version.jar cu.ftpd.user.userbases.UserbaseConverter <glftpd_root_dir> <cuftpd_data_dir>
This will read the users and groups from glftpd and create the equivalent files in the cuftpd datadir.
The normal glftpd_root_dir is "/glftpd", and the normal cuftpd data dir is "/cuftpd/data".
You will need to have cuftpd.jar in the same directory as converter.jar

--- Taking a directory structure and creating a dirlog and dupelog from it ---

For this purpose, there is a logcreator.jar file. Run it with "java -cp cuftpd-$version.jar cu.ftpd.logging.LogCreator", and it will
show you how to use it. Basically it wants these parameters:

$1 = path to the (new) dirlog
$2 = path to the (new) dupelog
$3 = append logs (true or false)
$4 = root, the directory to index

NOTE: It will not write to the dirlog until the whole scan is over, but the dupelog will be appended
"as it happens".
